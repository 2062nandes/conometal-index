<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Conservadoras</h1>
			</div>
		</section>
		<section class="container">
			<div class="items">
				<figure>
					<img src="/images/conservadora10lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Conservadoras HELATODO 10 lts.</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Medidas:</p>
						<p>Largo: 300 mm</p>
						<p>Alto: 400 mm</p>
						<p>Ancho: 180 mm </p>
						<p>Capacidad de 2 botellas de 2.5 lts.</p>
						<p>Conserva el frío por 12 hs</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/conservadora28lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Conservadoras HELATODO 28 lts.</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Medidas:</p>
						<p>Largo: 450 mm</p>
						<p>Alto: 420 mm</p>
						<p>Ancho: 320 mm </p>
						<p>Con tapón de desagote, que permite su fácil limpieza  </p>
						<p>Conserva el frío por 12 hs.</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/conservadora50lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Conservadoras HELATODO 50 lts.</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Medidas:</p>
						<p>Largo: 600 mm</p>
						<p>Alto: 450 mm</p>
						<p>Ancho: 450 mm </p>
						<p>Ideal Para nautica</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

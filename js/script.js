$(document).on('ready',inicio);
function inicio(){
	$('#btnCorreo').on('click', btnCorreo);
}
function btnCorreo(){
	validacion_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	$('.error').remove();
	$('.nombre, .fono, .cel, .ciudad, .dire, .mensaje').keyup(function(){
		if($(this).val() != ""){
			$('.error').fadeOut();
			return false;
		}
	});
	$('.email').keyup(function(){
		if($(this).val() != "" && validacion_email.test($(this).val())){
			$('.error').fadeOut();
			return false;
		}
	});
	if($('.nombre').val() == ''){
		$('.nombre').focus().after("<span class='error'>Introduzca su nombre</error>");
		return false;
	}else if($('.fono').val() == ''){
		$('.fono').focus().after("<span class='error'>Introduzca su teléfono</span>");
		return false;
	}else if($('.cel').val() == ''){
		$('.cel').focus().after("<span class='error'>Introduzca su número celular</span>");
		return false;
	}else if($('.ciudad').val() == ''){
		$('.ciudad').focus().after("<span class='error'>Introduzca su ciudad</span>");
		return false;
	}else if($('.dire').val() == ''){
		$('.dire').focus().after("<span class='error'>Introduzca su dirección</span>");
		return	false;
	}else if($('.email').val() == '' || !validacion_email.test($('.email').val())){
		$('.email').focus().after("<span class='error'>Correo incorrecto</span>");
		return false;
	}else if($('.mensaje').val() == ''){
		$('.mensaje').focus().after("<span class='error'>Escribir mensaje</span>");
		return	false;
	}else{
		$.ajax({
			type: 'POST',
			url: 'correo.php',
			data: $('#fCorreo').serialize(),
			success: function(data){
				alert(data);
				$('form')[0].reset();
			}
		});
		return false;
	}
}
		<section class="container">
			<div class="col col12 representaciones">
				<h3>Representaciones exclusivas de compañías asiáticas y sudamericanas para latinoamérica</h3>
			</div>
		</section>
		<section class="container productos">
			<figure class="col col3">
				<img src="/images/cocinas01.jpg" alt="">
				<figcaption>
					<h3>Cocinas</h3>
					<a href="cocinas.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/quemadores01.jpg" alt="">
				<figcaption>
					<h3>Accesorios de cocina</h3>
					<a href="accesorios.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/utensilloscocina01.jpg" alt="">
				<figcaption>
					<h3>Utensilios de cocina</h3>
					<a href="utensilios.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/estufacuarzo01.jpg" alt="">
				<figcaption>
					<h3>Estufas</h3>
					<a href="estufas.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/calefon01.jpg" alt="">
				<figcaption>
					<h3>Calefones</h3>
					<a href="calefones.php">Más detalles...</a>
				</figcaption>
			</figure>

			<figure class="col col3">
				<img src="/images/jardineria01.jpg" alt="">
				<figcaption>
					<h3>Jardinería</h3>
					<a href="jardineria.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/conservadora01.jpg" alt="">
				<figcaption>
					<h3>Conservadoras</h3>
					<a href="conservadoras.php">Más detalles...</a>
				</figcaption>
			</figure>
			<figure class="col col3">
				<img src="/images/variedades01.jpg" alt="">
				<figcaption>
					<h3>Variedades</h3>
					<a href="variedades.php">Más detalles...</a>
				</figcaption>
			</figure>
		</section>
	</section>
	<footer class="container">
		<div class="main" style="box-shadow:none;">
			<div class="col col12">
				<h4>Dirección</h4>
				<p>Santa Cruz, Z. Norte, 2º Anillo, Av. Cristóbal de Mendoza # 288 (entre Comando de Policía y el Cristo)</p>
				<p>Telf.: 3394396 – 3394373</p>
				<p>Cel.: 70946890 – 72605636</p>
				<p>lupe.conometal@hotmail.com</p>
			</div>
			<div class="col col12">
				<h4>CONOMETAL BOLIVIA <sup>©</sup> 2017</h4>
				<ul class="social_icons">
					<li><a href="https://www.facebook.com/search/top/?q=conometal%20bolivia" target="_blank" alt="Síguenos en Facebook"><i class="icon-facebook"></i></a></li>
					<li><a href="#" alt="Síguenos en Twitter"><i class="icon-youtube"></i></a></li>
				</ul>
				<h5>Todos los Derechos Reservados <sup>®</sup></h5>
				<p>Diseño y Programación<a href="http://www.ahpublic.com" target="_blank"> AH! PUBLICIDAD</a></p>
			</div>
		</div>
	</footer>
</body>
</html>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>CONOMETAL BOLIVIA. Artefactos y repuestos para cocinas, estufas y calefones a gas. </title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="Cocinas, estufas eléctricas y alógenas, calefones. Accesorios de cocina: Reguladores, pernos, quemadores, llaves hechizas. Jardinería, conservadoras, utensilios de cocina.">
	<meta name="designer" content="Fernando Javier Averanga Aruquipa. Gitlab:@2062nandes ">
	<link rel="shortcut icon"  href="/favicon.ico">
	<!--[if lt IE 9]>
		<script type="text/javascript" src="/js/html5shiv.min.js"></script>
		<script type="text/javascript" src="/js/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/engine1/style.css" />
	<link rel="stylesheet" href="/fonts/style.css">
	<link rel="stylesheet" href="/css/style.css">
	<script src="/js/prefixfree.min.js"></script>
	<script src="/js/jquery.min.js"></script>
	<script src="/js/script.js"></script>
</head>
<body>
	<section class="main">
		<header class="container">
			<div class="col col5">
				<img src="/images/logo.gif" alt="Conometal Bolivia">
			</div>
			<div class="col col5 ocultar-solo-tablet">
				<img src="/images/slogan.gif" alt="">
			</div>
			<div class="col col2">
				<div class="mnutop">
					<a href="/index.php">Inicio</a>
					<a href="/nosotros.php">Nosotros</a>
					<a href="/contactos.php">Contactos</a>
				</div>
				<a class="menu-promo" href="/promociones-y-ofertas.php">
					<span class="promo-text">PROMOCIONES Y OFERTAS</span>
					<img class="star-icon" src="/images/star.png" alt="star">
					<!-- <span class="star-icon">⋆</span> -->
				</a>
			</div>
		</header>
		<nav class="container contenedor">
			<a href="/cocinas.php">Cocinas</a>
			<a href="/accesorios.php">Accesorios de cocina</a>
			<a href="/utensilios.php">Utensilios de cocina </a>
			<a href="/estufas.php">Estufas</a>
			<a href="/calefones.php">Calefones</a>
			<a href="/jardineria.php">Jardinería</a>
			<a href="/conservadoras.php">Conservadoras</a>
			<a href="/variedades.php">Variedades</a>
		</nav>

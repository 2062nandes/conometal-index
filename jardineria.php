<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Jardinería</h1>
			</div>
		</section>
		<section class="container">
			<div class="items green-background">
				<figure>
					<img src="/images/bordeadora01.jpg">
					<figcaption>
						<div class="row">
							<h4>Bordeadora Schafer</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Bigua </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/enrrollador01.jpg">
					<figcaption>
						<div class="row">
							<h4>Enrollador para manguera</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/pistola7vias.jpg">
					<figcaption>
						<div class="row">
							<h4>Pistola pulverizadora</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>7 VIAS</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/lanza01.jpg">
					<figcaption>
						<div class="row">
							<h4>Lanza c/conector rápido </h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Con boquilla ajustable</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/rociador2d.jpg">
					<figcaption>
						<div class="row">
							<h4>Rociador impulso 2 direcciones  </h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Base trineo</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/rociador3d.jpg">
					<figcaption>
						<div class="row">
							<h4>Rociador 3 brazos ajustable </h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Base trineo</p>
					</figcaption>
				</figure>

			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

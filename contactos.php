<?php include_once 'include/header.php'; ?>
		<section class="container productos">
			<div class="col col4">
				<h3 class="title">Contactos</h3>
				<form action="#" method="post" id="fCorreo" class="form">
						<input type="text" class="form-input nombre" name="nombre" placeholder="Nombre completo..." required>
						<input type="text" class="form-input fono" name="fono" placeholder="Número de Telefóno..." >
						<input type="text" class="form-input cel" name="cel" placeholder="Número telefóno móvil..." required>
						<input type="text" class="form-input ciudad" name="ciudad" placeholder="Ciudad..">
						<input type="text" class="form-input dire" name="dire" placeholder="Dirección...">
						<input type="text" class="form-input email" name="email" placeholder="micorreo_electronico@es.com" required>
						<textarea class="form-textarea mensaje" name="mensaje" cols="35" rows="5" required placeholder="Mensaje..."></textarea>
		                <button type="button" class="form-submit" id="btnCorreo">Enviar</button>	
						<button type="reset" class="form-submit" >Limpiar</button>
				</form>
			</div>
			<div class="col col8">
				<h3 class="title">Mapa de ubicación</h3>
				<iframe src="https://www.google.com/maps/d/embed?mid=1SGlDa7Y6Cd-EylxfHgEGQBxohis" width="100%" height="370"></iframe>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>
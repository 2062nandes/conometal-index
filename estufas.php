<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Estufas</h1>
			</div>
		</section>
		<section class="container">
			<div class="items purpure-background">
				<figure>
					<img src="/images/estufacuarzo01.jpg">
					<figcaption>
						<div class="row">
							<h4>Estufa de Cuarzo vertical</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Eléctrica</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/estufaalogena01.jpg">
					<figcaption>
						<div class="row">
							<h4>Estufa Halógena</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/quemadores1500kcal.jpg">
					<figcaption>
						<div class="row">
							<h4>Pantalla 1500 calorías con válvula.</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Para Gas envasado y gas natural</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/quemadores3000kcal.jpg">
					<figcaption>
						<div class="row">
							<h4>Pantalla 3000 calorías con valvula y piloto</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Para Gas envasado y gas natural</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/quemadores1500-3000.jpg">
					<figcaption>
						<div class="row">
							<h4>Pantalla 1500-3000 calorías sin válvula </h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Para Gas envasado y gas natural</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Cocinas</h1>
			</div>
		</section>
		<section class="container">
			<div class="items teal-background">
				<figure>
					<img src="/images/cocina2h.jpg">
					<figcaption>
						<div class="row">
							<h4>Cocina de 2 hornallas con horno</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Mechero de Aluminio</p>
						<p>1800 calorías por hora </p>
						<p>gas natural y envasado</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/cocina4h.jpg">
					<figcaption>
						<div class="row">
							<h4>Cocina de 4 hornallas con horno</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Mechero de Aluminio</p>
						<p>1800 calorías por hora </p>
						<p>gas natural y envasado</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/cocina2htapa.jpg">
					<figcaption>
						<div class="row">
							<h4>Cocina </h4>
							<span class="precio">Bs. </span>
						</div>
						<p>2 hornallas</p>
						<p>con tapa</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/paelleros02.jpg">
					<figcaption>
						<div class="row">
							<h4>Quemador Paellero</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Diámetro 47 cm</p>
						<p>4 aros y 2 llaves </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/sartenes-paelleros.jpg">
					<figcaption>
						<div class="row">
							<h4>Sartenes Paelleros</h4>
							<span class="precio">Bs. </span>
						</div>
						<!-- <p>Medidas: 32x36x10 cm .</p> -->
					</figcaption>
				</figure>
				<figure>
					<img src="/images/cocinabutano.jpg">
					<figcaption>
						<div class="row">
							<h4>Cocina de camping a gas butano</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Medidas: 32x36x10 cm .</p>
						<p>Colores: Rojo o Beige</p>
						<p>Gas: Cartuchos de Gas Butano </p>
						<p>Consumo: 160 Grs, por Hora</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/paelleros01.jpg">
					<figcaption>
						<div class="row">
							<h4>Quemador Paellero</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>38 cm de diámetro</p>
						<p>3 aros y 2 llaves</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

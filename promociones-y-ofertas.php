<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Promociones y Ofertas</h1>
			</div>
		</section>
		<section class="container">
			<div class="items">
				<figure>
					<div class="promocion">
						<h5>%</h5>
						<p>DESCUENTO DEL</p>
					</div>
					<img src="/images/calefon14lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Calefón de 14 Lts, digital</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<div class="promocion">
						<h5>%</h5>
						<p>DESCUENTO DEL</p>
					</div>
					<img src="/images/calefon6lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Calefón de 10 Lts, digital</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<div class="promocion">
						<h5>%</h5>
						<p>DESCUENTO DEL</p>
					</div>
					<img src="/images/calefon10lts.jpg">
					<figcaption>
						<div class="row">
							<h4>Calefón de 6 Lts</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

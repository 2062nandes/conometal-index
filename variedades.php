<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Variedades</h1>
			</div>
		</section>
		<section class="container">
			<div class="items teal-background">
				<figure>
					<img src="/images/grifobronce.jpg">
					<figcaption>
						<div class="row">
							<h4>Grifos de bronce y pvc</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/farolesagas.jpg">
					<figcaption>
						<div class="row">
							<h4>Faroles a gas</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/braserodemesa03.jpg">
					<figcaption>
						<div class="row">
							<h4>Brasero de mesa inoxidable</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/pailarectangular.jpg">
					<figcaption>
						<div class="row">
							<h4>Paila rectangular hierro enlozado</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/pailacircular.jpg">
					<figcaption>
						<div class="row">
							<h4>Paila circular enlozada</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<!--
				<figure>
					<img src="/images/braserodemesa02.jpg">
					<figcaption>
					<div class="row">
						<h4>Brasero de mesa inoxidable</h4>
						<span class="precio">Bs. </span>
					</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>-->
				<figure>
					<img src="/images/lubricante01.jpg">
					<figcaption>
						<div class="row">
							<h4>Lubricante</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/repelente01.jpg">
					<figcaption>
						<div class="row">
							<h4>Repelente</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/dispensadores01.jpg">
					<figcaption>
						<div class="row">
							<h4>Dispensadores</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/Sopletesgasbutano01.jpg">
					<figcaption>
						<div class="row">
							<h4>Soplete a gas butano</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/ventilador01.jpg">
					<figcaption>
						<div class="row">
							<h4>Ventilador</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/garrafa45y10kg.jpg">
					<figcaption>
						<div class="row">
							<h4>Garrafa 45 y 10 kg</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/garrafa2-3kg.jpg">
					<figcaption>
						<div class="row">
							<h4>Garrafa 2 y 3 kg</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Accesorios de cocina</h1>
			</div>
		</section>
		<section class="container">
			<div class="items green-background">
				<figure>
					<img src="/images/rollos-de-maguera.jpg">
					<figcaption>
						<div class="row">
							<h4>Rollos de Maguera</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Transparente y negra x 25 x 50 y 100 mts</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/reguladores01.jpg">
					<figcaption>
						<div class="row">
							<h4>Reguladores de Gas</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/reguladores02.jpg">
					<figcaption>
						<div class="row">
							<h4>Reguladores de Gas</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>&nbsp;</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/quemadores01.jpg">
					<figcaption>
						<div class="row">
							<h4>Quemadores de Gas</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>Quemadores de gas largo</p>
						<p>Quemadores de gas corto</p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

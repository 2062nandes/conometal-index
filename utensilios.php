<?php include_once 'include/header.php'; ?>
		<section class="container">
			<div class="col col12">
				<h1 class="title">Utensilios de cocina - KITCHEN FLOWER</h1>
			</div>
		</section>
		<section class="container">
			<div class="items gold-background">
				<figure>
					<img src="/images/NY-1433.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1433</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA PLATINA-TOP SPLY 24CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1434.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1434</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA PLATINA TOP SPLY 24CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1409.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1409</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA TRI-PLY PREMIUM COOKWARE</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2588.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2588</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA PLATINA NOUVEAU TRI-PLY 26CM PREMIUM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2857.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2857</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA BELLY18-8 STANLASS STEEL24CM ACERO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2858.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2858</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA BELLY18-8 STAINLESS STEEL28CM ACERO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2879.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2879</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA PLUTO STOCK 18-8 ACERO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1543.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1543</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA PANSY 24 CM TRIPLY BOTON</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2915.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2915</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN CON TAPA COOL INDUCTION 18 CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2916.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2916</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA TAPA VIDRIO 18CM COOL INDUCTION</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2918.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2918</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA TAPA VIDRIO 24CM COOL INDUCTION</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2917.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2917</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>OLLA 2917 24CM COOL INDUCTION</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2147.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2147</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECO COOK MINI 16CM BANANA YELLOW</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2148.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2148</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECCO 2148 MINI VERDE 20CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1531.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1531</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECCO COOK CERAMIC COATING18CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1532.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1532</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECO COOK CERAMIC COATING20 CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1533.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1533</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECO COOK CERAMIC COATING 24 CM PREMIUM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1534.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1534</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>ECO COOK CERAMIC COATING 26CM  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2877.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2877</h4>
							<span class="precio">Bs. </span>
						</div>
						<p> COOKWARE JGO. 3 PZASCOLORES PREMIUM  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2968.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2968</h4>
							<span class="precio">Bs. </span>
						</div>
						<p> SET 3 PZAS CERAMIC COOKWARE ROSA  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/KCJ-H06MD.jpg">
					<figcaption>
						<div class="row">
							<h4>KCJ-H06MD</h4>
							<span class="precio">Bs. </span>
						</div>
						<p> ELECTRIC RICE COOKER 6 PORCIONES  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/KCJ-H10MD.jpg">
					<figcaption>
						<div class="row">
							<h4>KCJ-H10MD</h4>
							<span class="precio">Bs. </span>
						</div>
						<p> ELECTRIC RICE COOKER 10 PORCIONES  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1145.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1145</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PLATINA PREMIUM 20 CM.  </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1141.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1141</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PLATINA PREMIUM 20 CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1142.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1142</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PLATINA PREMIUM 28 CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2033.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2033</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN REVO DIAMOND 20CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2034.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2034</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN REVO DIAMOND 26CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2035.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2035</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN VERDE REVO DIAMOND 28CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2036.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2036</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN REVO DIAMOND 30CM VERDE</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2037.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2037</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN REVO DIAMOND 32CM VERDE</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2038.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2038</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN REVO DIAMOND 24 CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2039.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2039</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN WOK 28CM PREMIUM PAN</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2040.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2040</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN CUADRADO 22CM REVO DIAMOND</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2821.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2821</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN INOBLE PLOMO 28CM ANTIBAC.</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2822.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2822</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN INOBLE STONE 28 CM WOK PAN</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2823.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2823</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN INOBLE STONE22CM PLOMO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1856.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1856</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN ECO-COOK 20CM FRI-PAN</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1857.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1857</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN NEW-ECO-COOK26CM ANTIBACTERIAL</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1858.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1858</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN NEW ECO-COOK 28 CM ANTIBACTERIAL</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1859.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1859</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN NEW ECO-COOK 28 CM WOKPAN</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1864.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1864</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN NEW ECO-COOK 30 CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1865.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1865</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN NEW ECO-COOK 24 CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2941.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2941</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN MAXIMA 24CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2942.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2942</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN MAXIMA 28CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2943.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2943</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN MAXIMA PREMIUM 30CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2391.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2391</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PREMIUM INDUCTION FRYPAN 28CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2392.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2392</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PREMIUM INDUCTION FRY PAN</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2393.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2393</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PREMIUM INDUCTION WOK PAN24CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2394.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2394</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>SARTEN PREMIUM INDUCTION  WOKPAN 28CM ROJO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2125.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2125</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN ROSEE DIAMOND FRY PAN 26 CM</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2126.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2126</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN ROSEE DIAMOND COATING 28CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2127.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2127</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN ROSEE DIAMOND FRY PAN 30 CM FLORES</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2128.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2128</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN ROSEE DIAMOND FRY PAN 32CM FLORES</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2129.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2129</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN ROSEE DIAMOND WOK PAN 24CM FLORES</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2932.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2932</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN DIAMOND PAN 3D  30CM </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2934.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2934</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN DIAMOND PAN COATING 3D 26CM VINO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2935.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2935</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN DIAMOND PAN 3D 24CM VINO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2936.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2936</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p>SARTEN DIAMOND PAN 3D 24CM VINO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-2591.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-2591</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p> TOOT SET ECO COOK  7 PZAS</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/KF-2973.jpg">
					<figcaption>
						<div class="row">
							<h4>KF-2973</h4>
							<span class="precio">Bs. 200,00</span>
						</div>
						<p> SARTEN CON TAPA CUADRADO 24X20 CM VINO</p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/NY-1160.jpg">
					<figcaption>
						<div class="row">
							<h4>NY-1160</h4>
							<span class="precio">Bs. </span>
						</div>
						<p> CERASTONE PLANCHA REDONDA </p>
					</figcaption>
				</figure>
				<figure>
					<img src="/images/IG-370.jpg">
					<figcaption>
						<div class="row">
							<h4>IG-370</h4>
							<span class="precio">Bs. </span>
						</div>
						<p>PAILA CUADRADA CON ASAS 30CM  </p>
					</figcaption>
				</figure>
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>

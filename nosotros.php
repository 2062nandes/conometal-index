<?php include_once 'include/header.php'; ?>
		<section class="container productos">
			<div class="col col4">
				<h3 class="">Nosotros</h3>
				<h4 class="title">Misión</h4>
				<p>Ofrecer a nuestra clientela productos de calidad y especialmente satisfacer todas las necesidades y requerimientos que tengan para demostrar solidez, competitividad y ser una empresa líder en el mercado nacional, contamos con una gama completa de cocinas, accesorios, utensilios de cocina, estufas, calefones, conservadoras, todo en cuanto a jardinería y ferretería.</p>
				<h4 class="title">Visión</h4>
				<p>Crecer como una empresa líder a nivel nacional con un excelente servicio para cubrir las expectativas de nuestros clientes y tener los mejores productos en cuanto a calidad y marca.</p>
			</div>
			<div class="col col4">
				<br><br><br>
				<img src="/images/24739_1359095384407_7039104_n.jpg">
			</div>
			<div class="col col4">
				<br><br><br>
				<img src="/images/1935782_1241438803066_7113187_n.jpg">
			</div>
		</section>
<?php include_once 'include/footer.php'; ?>
